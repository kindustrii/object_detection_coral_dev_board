## Einleitung

Um KI-Modelle zu trainieren, wird in der Regel einiges an Rechenleistung benötigt, 
wenn das Training in einer akzeptablen Zeit durchgeführt werden soll.

Ein trainiertes Modell benötigt allerdings deutlich weniger Rechenleistung und lässt sich somit auch auf kleinen, sogenannten Einplatinen-Computern implementieren und verwenden.

Auf dem Bild siehst Du das **Coral Dev Board** von Google, auf dem wir unterschiedliche Modelle implementieren werden. 

![Coral Dev Board](https://coral.ai/static/docs/images/devboard/devboard-inhand.jpg)


